﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Fabric;
using System.Linq;
using System.Threading.Tasks;

namespace WebConsole.Data.Models
{
    [Table("ScheduleTasks")]
    public class ScheduleTask
    {
        public Guid Id { get; set; }
        public Guid ScheduleId { get; set; }
        public Schedule Schedule { get; set; }
    }
}
