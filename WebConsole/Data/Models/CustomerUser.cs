﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using WebConsole.Models;

namespace WebConsole.Data.Models
{
    [Table("CustomerUsers")]
    public class CustomerUser
    {
        public Guid CustomerId { get; set; }

        public Customer Customer { get; set; }

        public Guid UserId { get; set; }

        public ApplicationUser User { get; set; }
    }
}
