﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebConsole.Data.Models
{
    [Table("Schedules")]
    public class Schedule
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        public Guid WebResourceId { get; set; }

        public WebResource WebResource { get; set; }

        public ICollection<ScheduleTask> Tasks { get; set; }
    }
}
