﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebConsole.Data.Models
{
    [Table("Countries")]
    public class Country
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [Required]
        [StringLength(16)]
        public string ShortName { get; set; }

        [Required]
        public int PhoneCode { get; set; }

        public ICollection<State> States { get; set; }
    }
}
