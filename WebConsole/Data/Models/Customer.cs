﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebConsole.Data.Models
{
    [Table("Customers")]
    public class Customer
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        [Required]
        [StringLength(128)]
        public string Email { get; set; }

        [Required]
        [StringLength(128)]
        public string Phone { get; set; }

        public Guid StateId { get; set; }

        public State State { get; set; }

        [Required]
        [StringLength(256)]
        public string Address { get; set; }

        [Required]
        [StringLength(256)]
        public string Address2 { get; set; }

        [Required]
        [StringLength(256)]
        public string ContactPerson { get; set; }

        [Required]
        [StringLength(16)]
        public string ZipCode { get; set; }

        public bool IsActive { get; set; }

        public DateTime Created { get; set; }

        public ICollection<CustomerUser> CustomerUsers { get; set; }

        public ICollection<WebResource> WebResources { get; set; }
    }
}
