﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebConsole.Data.Models
{
    [Table("WebResources")]
    public class WebResource
    {
        [Key]
        public Guid Id { get; set; }

        public Guid CustomerId { get; set; }

        public Customer Customer { get; set; }

        [Required]
        [StringLength(256)]
        public string Name { get; set; }

        [Required]
        public string Url { get; set; }

        public DateTime Created { get; set; }

        public ICollection<Schedule> Schedules { get; set; }
    }
}
