﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebConsole.Data.Models
{
    [Table("States")]
    public class State
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public Guid CountryId { get; set; }

        public Country Country { get; set; }

        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        public ICollection<Customer> Customers { get; set; }
    }
}
