﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using WebConsole.Data.Models;
using WebConsole.Models;

namespace WebConsole.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, IdentityRole<Guid>, Guid>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }



        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ApplicationUser>().ToTable("Users", "dbo");
            builder.Entity<IdentityRole<Guid>>().ToTable("Roles", "dbo");
            builder.Entity<IdentityRoleClaim<Guid>>().ToTable("RoleClaims", "dbo");
            builder.Entity<IdentityUserRole<Guid>>().ToTable("UserRoles", "dbo");
            builder.Entity<IdentityUserClaim<Guid>>().ToTable("UserClaims", "dbo");
            builder.Entity<IdentityUserLogin<Guid>>().ToTable("UserLogins", "dbo");
            builder.Entity<IdentityUserToken<Guid>>().ToTable("UserTokens", "dbo");

            builder.Entity<State>(b =>
            {
                b.HasOne(m => m.Country)
                    .WithMany(m => m.States)
                    .HasForeignKey(m => m.CountryId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<Country>();

            builder.Entity<Customer>(b =>
            {
                b.HasOne(m => m.State)
                    .WithMany(m => m.Customers)
                    .HasForeignKey(m => m.StateId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<CustomerUser>(b =>
            {
                b.HasKey(t => new { t.CustomerId, t.UserId });

                b.HasOne(pt => pt.User)
                    .WithMany(p => p.CustomerUsers)
                    .HasForeignKey(pt => pt.UserId);

                b.HasOne(pt => pt.Customer)
                    .WithMany(p => p.CustomerUsers)
                    .HasForeignKey(pt => pt.CustomerId);
            });

            builder.Entity<WebResource>(b =>
            {
                b.HasOne(m => m.Customer)
                    .WithMany(m => m.WebResources)
                    .HasForeignKey(m => m.CustomerId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<Schedule>(b =>
            {
                b.HasOne(m => m.WebResource)
                    .WithMany(m => m.Schedules)
                    .HasForeignKey(m => m.WebResourceId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            builder.Entity<ScheduleTask>(b =>
            {
                b.HasOne(m => m.Schedule)
                    .WithMany(m => m.Tasks)
                    .HasForeignKey(m => m.ScheduleId)
                    .OnDelete(DeleteBehavior.Cascade);
            });
        }
    }
}
